package com.company;

/**
#3 Посчитайте ​​факториал ​​20, ​​и ​​выведите ​​результат ​​в ​к​онсоль.
*/

public class Main {

    public static void main(String[] args) {

        // big int или long? long хватит
        long k = 20;
        if (k < 0)
            System.out.print("Введите положительное число!");
        else System.out.print("Факториал " + k + " равен " + factorial(k));

    }


    public static long factorial(long n){

        // Рекурсивный вариант
        if ( n == 0) return 1;
        return n*factorial(n-1);

    }
}
